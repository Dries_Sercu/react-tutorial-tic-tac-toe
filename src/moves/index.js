import React from 'react';
import './index.css';

export default class Moves extends React.Component {
	getPosition(position){
		const positions = [
			'(1,1)',
			'(1,2)',
			'(1,3)',
			'(2,1)',
			'(2,2)',
			'(2,3)',
			'(3,1)',
			'(3,2)',
			'(3,3)',
		];

		return positions[position];
	}
	render() {
		const history = this.props.history;
	    const moves = history.map((step, index) => {
	    	let move;
	    	if(this.props.sortAscending){
	    		move = index;
	    	} else {
	    		move = history.length - index -1;
	    	}

	    	let desc;
	    	if(move){
	    		desc = 'Go to move #' + move + ' ' + this.getPosition(step.changedSquarePosition);
	    	} else {
	    		desc = 'Go to game start';
	    	}

	    	let className = '';
	    	if(this.props.activeMove === index){
	    		className += 'bold';
	    	}

			return (
				<li key={move}>
				  <button
				  	className={className}
				  	onClick={() => this.props.onClick(index)}
				  	>{desc}</button>
				</li>
			);
		});

		return (
			<ol>{moves}</ol>
		);
	}
}
