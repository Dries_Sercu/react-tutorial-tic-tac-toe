import React from 'react';
import Square from '../square';

export default class Board extends React.Component {
	renderSquare(squareId) {
		const highlight = this.props.highlight ? this.props.highlight.indexOf(squareId) > -1 : false;
		return (
			<Square
				key={squareId}
				highlight={highlight}
				value={this.props.squares[squareId]}
				onClick={() => this.props.onClick(squareId)}
			/>
		);
	}

	render() {
		let squareCounter = 0;
		return (
			<div>{
				[1,2,3].map( (row) => {
					return <div key={row} className="board-row">{
						[1,2,3].map( (col) => {
							squareCounter++;
							return this.renderSquare(squareCounter - 1);
						})
					}</div>
				})
			}</div>
		);
	}
}
