import React from 'react';
import Board from '../board';
import Moves from '../moves';

export default class Game extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			history : [{
				squares: Array(9).fill(null),
			}],
			stepNumber: 0,
			xIsNext: true,
			sortAscending: true,
		}
		this.winningLines = [
		    [0, 1, 2],
		    [3, 4, 5],
		    [6, 7, 8],
		    [0, 3, 6],
		    [1, 4, 7],
		    [2, 5, 8],
		    [0, 4, 8],
		    [2, 4, 6],
	  	];
	}
	getNextSymbol(){
		return this.state.xIsNext? 'X' : 'O';
	}
	handleClick(i) {
		const history = this.state.history.slice(0, this.state.stepNumber + 1);
		const current = history[history.length - 1];
		const squares = current.squares.slice();
		if (this.calculateWinner(squares) || squares[i]) {
			return;
		}
		squares[i] = this.getNextSymbol();
		this.setState({
			history: history.concat({
				squares: squares,
				changedSquarePosition: i,
			}),
			stepNumber: history.length,
			xIsNext: !this.state.xIsNext
		});
	}
	jumpTo(step){
		this.setState({
			stepNumber: step,
			xIsNext: (step % 2) === 0,
		})
	}
	getStatus(squares) {
	    const winner = this.calculateWinner(squares);
	    if (winner) {
	      return 'Winner: ' + winner;
	    }
	    if(this.state.stepNumber === 9) {
	    	return 'This game is a draw';
	    }
	    return 'Next player: ' + (this.state.xIsNext ? 'X' : 'O');
	}
	calculateWinner(squares) {
		if(!this.getWinningSquares(squares)) return null;
		return squares[this.getWinningSquares(squares)[0]];
	}
	getWinningSquares(squares) {
		for (let i = 0; i < this.winningLines.length; i++) {
			const [a, b, c] = this.winningLines[i];
			if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
				return this.winningLines[i];
			}
		}
		return null;
	}
	changeSortMode() {
		this.setState({
			history: this.state.history.reverse(),
			stepNumber: this.state.history.length - this.state.stepNumber - 1,
			sortAscending: !this.state.sortAscending,
		});
	}

	render() {
	    const history = this.state.history;
	    const current = history[this.state.stepNumber];
	    const status = this.getStatus(current.squares);
	    const squaresToHighlight = this.getWinningSquares(current.squares);

		const sortMode = !this.state.sortAscending ? 'Ascending' : 'Descending';

		return (
			<div className="game">
				<div className="game-board">
					<Board
						highlight={squaresToHighlight}
						squares={current.squares}
						onClick={(i) => this.handleClick(i)} />
				</div>
				<div className="game-info">
					<div>{status}</div>
					<button
						onClick={() => this.changeSortMode()}
						>Sort {sortMode}</button>
					<Moves
						history={history}
						sortAscending={this.state.sortAscending}
						onClick={(i) => this.jumpTo(i)}
						activeMove={this.state.stepNumber}
						/>
				</div>
			</div>
		);
	}
}
