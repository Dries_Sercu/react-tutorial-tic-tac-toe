import React from 'react';
import './index.css';

export default function Square(props) {
	let className = 'square ';
	if(props.highlight){
		className += 'highlight ';
	}
	return (
		<button
			className={className}
			onClick={ props.onClick }
		>
			{props.value}
		</button>
	);
}
